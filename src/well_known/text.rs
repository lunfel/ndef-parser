use crate::ndef::{NDEF, WellKnownType};
use std::str::from_utf8;

#[derive(Debug)]
pub struct Text {
    control_byte: u8,
    lang: String,
    payload: String
}

impl Text {
    pub fn parse(record: &NDEF) -> Result<Self, &'static str> {
        if record.ndef_type != WellKnownType::Text {
            return Err("Can only parse Text ndef records");
        }

        let data = &record.data[..];

        if data.len() == 0 {
            return Err("Empty Text record");
        }

        let control_byte = &data[0];
        let iana_len: usize = *control_byte as usize & 0b00111111;
        let lang = from_utf8(&record.data[1..iana_len + 1]).unwrap();
        let payload = from_utf8(&record.data[1 + iana_len..]).unwrap();

        Ok(Self {
            control_byte: *control_byte,
            lang: lang.to_owned(),
            payload: payload.to_owned()
        })
    }
}
